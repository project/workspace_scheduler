<?php

namespace Drupal\workspace_scheduler\Commands;

use Drupal\Core\Cache\CacheTagsInvalidator;
use Drupal\Core\Database\Database;
use Drupal\Core\Session\AccountSwitcher;
use Drupal\workspaces\WorkspaceAssociationInterface;
use Drupal\workspaces\WorkspaceManager;
use Drupal\workspaces\WorkspaceOperationFactory;
use Drush\Commands\DrushCommands;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\user\Entity\User;

use Drupal\Core\Session\UserSession;
use Drupal\Core\Session\AccountSwitcherInterface;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
class WorkspaceSchedulerCommands extends DrushCommands
{

  /**
   * Echos back hello with the argument provided.
   *
   * @param string $name
   *   Name of the workspace to publish.
   *
   * @command workspace_scheduler:publish
   * @aliases publish
   * @options arr An option that takes multiple values.
   * @options msg Whether or not an extra message should be displayed to the user.
   * @usage workspace_scheduler:publish workspace
   *   Publish a give workspace.
   */
  public function publish($name, $options = ['msg' => false])
  {
    // We need to set a user with the right permissions.
    \Drupal::currentUser()->setAccount(user_load(1));

    $entity_manager = \Drupal::entityTypeManager();
    $workspace = $entity_manager->getStorage('workspace')->load($name);

    print_r($workspace);

    // Set current workspace.
    \Drupal::service('workspaces.manager')->setActiveWorkspace($workspace);

    $active_workspace = \Drupal::service('workspaces.manager')->getActiveWorkspace();

    // Deploy workspace.
    $workspace_publisher = \Drupal::service('workspaces.operation_factory')->getPublisher($active_workspace);
    // TODO: Check if the workspace is empty, so it can't be deployed.
    // At the moment that scenario throws an error.
    $workspace_publisher->publish();
  }
}
